package cricket;
import java.util.Iterator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

//import javax.swing.text.html.HTMLDocument.Iterator;

public class MainCricket {

	public static void main(String[] args)  {

		int choice;
		int id;
		String name;
		int matchPlayed;
		int totalRunsScored;
		int wicketsTaken;
		int outOnZeroScore;
		String playerType;
		//double averageScore;
		
		String n;
		

		HashMap<String, List<String>> map = new HashMap<>();
		ArrayList<Player> players = new ArrayList<>();
		List<String> pname = new ArrayList<>();
		ArrayList<Double> avgRuns = new ArrayList<>();
		HashMap<Double, List<String>> finalTeam = new HashMap<>();
	//	HashMap<Double, List<String>> ft = new HashMap<>();
		ArrayList<Double> ft = new ArrayList<>();
	

		Scanner sc= new Scanner(System.in);
		int bowlers;
        System.out.print("Enter the number of Bowlers , which should be greater than 3 : ");
        bowlers = sc.nextInt();
        int remain;

		do {
			System.out.println("1. Add Player Information");
			System.out.println("2. Update player informtion by name");
			System.out.println("3. Display Final Team ");
			System.out.println("4. Display All Players");
			System.out.println("5. Exit");
			System.out.println("Enter your choice ");
			choice = sc.nextInt();

			switch(choice) {

			case 1:
				for(int i=1;i<=5;i++) {
					try {
						System.out.println("enter id of player" + i);
						id = sc.nextInt();
						System.out.println(" id of player " + i + ": "+id);
						System.out.println("enter name of player" + i);
						sc.nextLine();
						name = sc.nextLine();
						System.out.println(" name of player" + i + ": "+name);
						System.out.println("No of Matches  played by player : " + i );
						matchPlayed = sc.nextInt();
						System.out.println(" No of Matches  played by player" + i + ": "+matchPlayed);
						System.out.println("enter Total Runs Scored by player" + i);
						totalRunsScored = sc.nextInt();
						System.out.println("Total Runs Scored by player"+ i + ": "+totalRunsScored);
						double averageScore =(totalRunsScored)/(matchPlayed);
						System.out.println("Average Score of player "+i+": "+averageScore);
						System.out.println("enter wickets taken by player" + i);
						wicketsTaken = sc.nextInt();
						System.out.println("Total wickets taken by player" + i + ": "+wicketsTaken);
						System.out.println("outonzeroscore" + i);
						outOnZeroScore = sc.nextInt();
						System.out.println("outonZeroScore by player" + i + ": "+outOnZeroScore);
						System.out.println("enter playertype" + i);
						sc.nextLine();
						playerType = sc.nextLine();
						System.out.println("player type" + i + ": "+playerType);

						players.add(new Player(id, name, matchPlayed, totalRunsScored, wicketsTaken, outOnZeroScore, playerType,averageScore));
						pname.add(name);
						ArrayList<String> substr = new ArrayList<String>();
							substr.add(Integer.toString(id));
							substr.add(name);
							substr.add(Integer.toString(matchPlayed));
							substr.add(Integer.toString(totalRunsScored));
							substr.add(Integer.toString(wicketsTaken));
							substr.add(Integer.toString(outOnZeroScore));
							substr.add(playerType);
							substr.add(Double.toString(averageScore));
						map.put(name, substr);
						avgRuns.add(averageScore);
						finalTeam.put(averageScore, substr);
					}
					catch(Exception e) {
						System.out.println(e);
					}
				
				}
				break;

			case 2:{
				int c;
				System.out.println("Enter name to get details");
				n = sc.next();
				List<String> playerinfo = map.get(n);
				System.out.println("Player Details before updating");
				System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------");
				System.out.printf("%2s%10s%18s%18s%18s%18s%18s%18s","ID","NAME","MATCH_PLAYED","TOTAL_RUNS","WICKETS","OUT_ON_ZERO","PLAYER_TYPE","AVG_SCORE");
				System.out.println();
				System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------");
				
				System.out.format("%2s%10s%18s%18s%18s%18s%18s%18s",playerinfo.get(0),playerinfo.get(1),playerinfo.get(2),playerinfo.get(3),playerinfo.get(4),playerinfo.get(5),playerinfo.get(6),Double.parseDouble(playerinfo.get(3))/Double.parseDouble(playerinfo.get(2)));
				System.out.println();
			
				System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------");
		
				do {
				System.out.println("1. Update ID");
				System.out.println("2. Update Name");
				System.out.println("3. Update Match Played");
				System.out.println("4. Update Total Runs");
				System.out.println("5. Update Wickets");
				System.out.println("6. Update Out on Zero");
				System.out.println("7. Update Player Type");
				System.out.println("8. Exit from Updating");
				System.out.println("Enter your choice ");
				c = sc.nextInt();
					
				List<String> x = playerinfo;
				avgRuns.remove(Double.parseDouble(playerinfo.get(3))/Double.parseDouble(playerinfo.get(2)));
				switch(c) {
				
				case 1:
					System.out.println("Enter new-ID");
					try {
						int newid = sc.nextInt();
						x.set(0, Integer.toString(newid));
						map.replace(n, x);
					}catch(Exception e) {
						
					}
					break;
					
				case 2:
					System.out.println("Enter new-Name");
					String newName = sc.next();
					x.set(1, newName);
					map.replace(n, x);
					break;
					
				case 3:
					System.out.println("Enter new-Matches Played");
					try {
						int newMp = sc.nextInt();
						x.set(2, Integer.toString(newMp));
						map.replace(n, x);
					}catch(Exception e) {
						
					}
					break;
								
				case 4:
					System.out.println("Enter new-Total Runs Scored");
					try {
						int newTRS = sc.nextInt();
						x.set(3, Integer.toString(newTRS));
						map.replace(n, x);
					}catch(Exception e) {
						
					}
					break;
							
				case 5:
					System.out.println("Enter new-Wickets taken");
					try {
						int newWt = sc.nextInt();
						x.set(4, Integer.toString(newWt));
						map.replace(n, x);
					}catch(Exception e) {
						
					}
					break;
							
				case 6:
					System.out.println("Enter new-Out on Zero");
					try {
						int newOZ = sc.nextInt();
						x.set(5, Integer.toString(newOZ));
						map.replace(n, x);
					}catch(Exception e) {
						
					}
					break;
							
				case 7:
					System.out.println("Enter new-Player type");
					try {
						String newPT = sc.next();
						x.set(6, newPT);
						map.replace(n, x);
					}catch(Exception e) {
						
					}
					break;
					
				case 8:
					break;
				}//Case 2-switch ends
				
				double avgrun = Double.parseDouble(x.get(3))/Double.parseDouble(x.get(2));
				x.set(7, Double.toString(avgrun));
				map.replace(n, x);
				finalTeam.replace(Double.parseDouble(playerinfo.get(3))/Double.parseDouble(playerinfo.get(2)), x);
				avgRuns.add(avgrun);
				}while(c!=8);
				
				
				break;
			}
			
			case 3:
				List<Player> temp = new ArrayList<Player>();
                List<Player> final_team = new ArrayList<Player>();
                temp = players;

                Collections.sort(temp, new AwgComparator());
                int final_bowlers;
               while(true) {
                	System.out.print("Enter the number of bowlers to be included in 11 memebers team out"  + " of "+bowlers+" members : ");
                    final_bowlers = sc.nextInt();
                   if(final_bowlers <= bowlers) {
                   	break;
                    }
                    else {
                    	System.out.println("ERROR\nENTER again which is less than or equal to number of total bowlers.");
                    }
                }
                
		
                for(int j=0; j<final_bowlers; j++){
                    Iterator<Player> itr61 = temp.iterator();
                    while(itr61.hasNext()){
                        Player play2 = itr61.next();
                        if(play2.getPlayerType().equals("Bowler")){
                            final_team.add(play2);
                            temp.remove(play2);
                            break;
                        }
                    }
                }

                Collections.sort(temp, new AwgComparator());
                Iterator<Player> itr4 = temp.iterator();
                int i = 11-3;
                while(itr4.hasNext()){
                    Player play4 = itr4.next();
                    i--;
                    final_team.add(play4);
                    if( i == 0){
                        break;
                    }
                }

                //Collections.sort(final_team, new NameComparator());
                Iterator<Player> itr5 = final_team.iterator();
                while(itr5.hasNext()){
                    Player play5 = itr5.next();
                    System.out.println("ID : "+play5.getId()+"   NAME : "+play5.getName()+"   Match Played : "+play5.getMatchPlayed()+
                            "   Total run scored : "+ play5.getTotalRunsScored()+"   Wickets Taken : "+play5.getWicketsTaken()+
                            "   Out on zero score : "+play5.getOutOnZeroScore()+"   Player type : "+play5.getPlayerType()+" AverageScore : "+play5.getAverageScore());
                }

                System.out.println(" ");
                break;


//				ArrayList<Player> a1 = new ArrayList<>();
//				ArrayList<Double> ar = new ArrayList<>();
//				ar.addAll(avgRuns);
//				Collections.sort(ar, Collections.reverseOrder());
//				System.out.println("Final Team");
//				System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------");
//				System.out.printf("%2s%10s%18s%18s%18s%18s%18s%18s","ID","NAME","MATCH_PLAYED","TOTAL_RUNS","WICKETS","OUT_ON_ZERO","PLAYER_TYPE","AVG_SCORE");
//				System.out.println();
//				System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------");
//	            Collections.sort(players,new AwgComparator());
//	            Iterator<Player> itr5 = players.iterator();
//                while(itr5.hasNext()){
//                    Player play5 = itr5.next();
//                    System.out.println(play5.getId()+play5.getName()+play5.getMatchPlayed()+
//                             play5.getTotalRunsScored()+play5.getWicketsTaken()+
//                           play5.getOutOnZeroScore()+play5.getPlayerType()+play5.getAverageScore());
//                }
//				for(int i=0;i<ar.size();i++) {
//					List<String> op = finalTeam.get(ar.get(i));
//						System.out.format("%2s%10s%18s%18s%18s%18s%18s%18s",op.get(0),op.get(1),op.get(2),op.get(3),op.get(4),op.get(5),op.get(6),Double.parseDouble(op.get(3))/Double.parseDouble(op.get(2)));
//						System.out.println();
//					
//				}
//				
//				System.out.println("------------------------------------------------------------------------------------------------------------------------------------------");
//				break;
                
				
			
			case 4:
				System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------");
				System.out.printf("%2s%10s%18s%18s%18s%18s%18s,%18s","ID","NAME","MATCH_PLAYED","TOTAL_RUNS","WICKETS","OUT_ON_ZERO","PLAYER_TYPE","AVG_SCORE");
				System.out.println();
				System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------");
				
				for(List<String> p : map.values()) {
					System.out.format("%2s%10s%18s%18s%18s%18s%18s%18s",p.get(0),p.get(1),p.get(2),p.get(3),p.get(4),p.get(5),p.get(6),Double.parseDouble(p.get(3))/Double.parseDouble(p.get(2)));
					System.out.println();
				}
				System.out.println("------------------------------------------------------------------------------------------------------------------------------------------");
				break;

			case 5:
				System.out.println("------------------- THANK YOU -------------------");
				break;
			}
			
		}while(choice!=5);
	}
	


}