package cricket;

import java.util.Comparator;

class AwgComparator implements Comparator<Player> {
	
	public int compare(Player e1, Player e2) {
		return (int)(e2.getAverageScore()- e1.getAverageScore());
	}
	
}
public class Player {

	private int id;
	private String name;
	private int matchPlayed;
	private int totalRunsScored;
	private int wicketsTaken;
	private int outOnZeroScore;
	private String playerType;
	double averageScore;
	
	//generating getter and setter methods
	
	public int getId() {
		return this.id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public double getAverageScore() {
		return averageScore;
	}

	public void setAverageScore(double averageScore) {
		this.averageScore = averageScore;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getMatchPlayed() {
		return this.matchPlayed;
	}
	
	public void setMatchPlayed(int matchPlayed) {
		this.matchPlayed = matchPlayed;
	}
	
	public int getTotalRunsScored() {
		return this.totalRunsScored;
	}
	
	public void setTotalRunsScored(int totalRunsScored) {
		this.totalRunsScored = totalRunsScored;
	}
	
	public int getWicketsTaken() {
		return this.wicketsTaken;
	}
	
	public void setWicketsTaken(int wicketsTaken) {
		this.wicketsTaken = wicketsTaken;
	}
	
	public int getOutOnZeroScore() {
		return this.outOnZeroScore;
	}
	
	public void setOutOnZeroScore(int outOnZeroScore) {
		this.outOnZeroScore = outOnZeroScore;
	}
	
	public String getPlayerType() {
		return this.playerType;
	}
	
	public void setPlayerType(String playerType) {
		this.playerType = playerType;
	}
	
	public Player() {
		super();
	}
	public Player(int id, String name, int mp, int trs, int wt, int ozs, String pt,Double avg) {
		//super();
		this.id = id;
		this.name = name;
		this.matchPlayed = mp;
		this.totalRunsScored = trs;
		this.wicketsTaken = wt;
		this.outOnZeroScore = ozs;
		this.playerType = pt;
		this.averageScore = avg;
	}
	
	
	
	
}