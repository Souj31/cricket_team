import java.io.*;
import java.util.*;


class Cricket implements Serializable
{
    int id;
    String name;
    int matchplayed;
    int totalrunsscored;
    int wicketstaken;
    int outonzeroscore;
    String playertype;
    double averagescore;


    Cricket(){}

    Cricket(int i,String n,int m,int t, int w, int o,String p,double a)
    {
        id =i;
        name=n;
        matchplayed=m;
        totalrunsscored =t;
        wicketstaken = w;
        outonzeroscore = o;
        playertype = p;
        averagescore =a;

    }


    public String toString()
    {
      return "ID : "+id+"Player Name : "+name+"Matches Played : "+matchplayed+"Total Runs Scored : "+totalrunsscored+"Wickets Taken : "+wicketstaken+"Out on Zeros : "+outonzeroscore+"Player Type : "+playertype+"Average Score : "+averagescore;
    }
}

public class Linked_List {
    public static void main(String[] args) throws IOException {
        List<String> al = new ArrayList<>(20);

        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter the names of players");
        Cricket p = null;
        HashMap<String, Cricket> hm = new HashMap<>();
        try {
            FileInputStream fis = new FileInputStream("Team");
            ObjectInputStream ois = new ObjectInputStream(fis);


            int count = ois.readInt();
            for (int i = 1; i < count; i++) {
                p = (Cricket) ois.readObject();
                System.out.println(p);
                hm.put(p.name, p);
            }
            fis.close();
            ois.close();

        } catch (Exception e) {

        }

        FileOutputStream fos = new FileOutputStream("Team Players");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        System.out.println("Menu");

        int choice;
        int id;
        String name;
        int matchplayed;
        int totalrunsscored;
        int wicketstaken;
        int outonzeroscore;
        String playertype;
        double averagescore;


        do {
            System.out.println("1. Display All Players");
            System.out.println("2. Update Player Information by name");
            System.out.println("3. Display Final Team ");
            System.out.println("4. Add Player Information");
            System.out.println("5. Exit");
            System.out.println("Enter your choice ");
            choice = sc.nextInt();
            //sc.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
            switch (choice) {
//                case 1:
//                    System.out.println("Enter player information by name");
//                    for(int i = 1; i<=5; i++)
//                    {
//                        System.out.println("enter id of player" + i);
//                        id = sc.nextInt();
//                        System.out.println(" id of player " + i + ": "+id);
//                        System.out.println("enter name of player" + i);
//                        sc.nextLine();
//                        name = sc.nextLine();
//                        System.out.println(" name of player" + i + ": "+name);
//                        System.out.println("No of Matches  played by player : " + i );
//                        matchplayed = sc.nextInt();
//                        System.out.println(" No of Matches  played by player" + i + ": "+matchplayed);
//                        System.out.println("enter Total Runs Scored by player" + i);
//                        totalrunsscored = sc.nextInt();
//                        System.out.println("Total Runs Scored by player"+ i + ": "+totalrunsscored);
//                        averagescore =(totalrunsscored)/(matchplayed);
//                        System.out.println("Average Score of player "+i+": "+averagescore);
//                        System.out.println("enter wickets taken by player" + i);
//                        wicketstaken = sc.nextInt();
//                        System.out.println("Total wickets taken by player" + i + ": "+wicketstaken);
//                        System.out.println("outonzeroscore" + i);
//                        outonzeroscore = sc.nextInt();
//                        System.out.println("outonZeroScore by player" + i + ": "+outonzeroscore);
//                        System.out.println("enter playertype" + i);
//                        sc.nextLine();
//                        playertype = sc.nextLine();
//                        System.out.println("player type" + i + ": "+playertype);
//                        System.out.println("ID : " + id  +";"+" Name : " + name +"; "+ " Matches Played : " + matchplayed +"; "+" Total Runs Scored : " + totalrunsscored +" ;"+ " Wickets Taken : " + wicketstaken +" ;" +" Out on Zero Score : " + outonzeroscore + ";"+" Player Type : " + playertype+" ;"+" Average Score : "+averagescore);
//                    }
//
//
//                    break;

                case 1: System.out.println("All Player iformation");

                HashMap<Integer, String> pl = new HashMap<>();
                    Pl player1 = new Pl(1,"Shikhar Dhawan","Batsman",0.0,70500,400,25,32);
                    Pl player2 = new Player(2,"Virat Kohli","Batsman",0.0,59500,400,25,32);
                    Pl player3 = new Player(3,"Ravindra Jadeja","All-Rounder",0.0,10500,400,25,32);
                    Player player4 = new Player(4,"Mohammed Shami","Bowler",0.0,15500,400,25,32);
                    Player player5 = new Player(5,"Manish Pandey","Bowler",0.0,13500,400,25,32);
                    Player player6 = new Player(6,"Jasprit Bumrah","Bowler",0.0,38500,400,25,32);
                    Player player7 = new Player(19,"Mayank Agarwal","Batsman",0.0,29500,400,25,32);
                    Player player8 = new Player(8,"Shreyas Iyer","Batsman",0.0,91500,400,25,32);
                    Player player9 = new Player(9,"Yuzvendra Chahal","Bowler",0.0,85500,400,25,32);
                    Player player10 = new Player(10,"KL Rahul","Wicket-Keeper",0.0,75500,400,25,32);
                    Player player11 = new Player(11,"Hardik Pandya","Batsman",0.0,92500,400,25,32);
                    Player player12 = new Player(12,"Kuldeep Yadav","Bowler",0.0,16500,400,25,32);
                    Player player13 = new Player(13,"Shardul Thakur","Bowler",0.0,7500,400,25,32);
                    Player player14 = new Player(14,"Navdeep Saini","Bowler",0.0,8500,400,25,32);
                    Player player15 = new Player(15,"Shubman Gill","Batsman",0.0,72500,400,25,32);
                    Player player16 = new Player(16,"Ajinkya Rahane","Batsman",0.0,62500,400,25,32);
                    Player player17 = new Player(17,"Ravichandran Ashwin","Bowler",0.0,22500,400,25,32);
                    Player player18 = new Player(18,"Umesh Yadav","Bowler",0.0,32500,400,25,32);
                    Player player19 = new Player(7,"MS Dhoni","Wicket-Keeper",0.0,98500,400,25,32);
                    Player player20 = new Player(20,"Hanuma Vihari","Batsman",0.0,52500,400,25,32);

                    allPlayers.put(player1.getName(),player1);
                    allPlayers.put(player2.getName(),player2);
                    allPlayers.put(player3.getName(),player3);
                    allPlayers.put(player4.getName(),player4);
                    allPlayers.put(player5.getName(),player5);
                    allPlayers.put(player6.getName(),player6);
                    allPlayers.put(player7.getName(),player7);
                    allPlayers.put(player8.getName(),player8);
                    allPlayers.put(player9.getName(),player9);
                    allPlayers.put(player10.getName(),player10);
                    allPlayers.put(player11.getName(),player11);
                    allPlayers.put(player12.getName(),player12);
                    allPlayers.put(player13.getName(),player13);
                    allPlayers.put(player14.getName(),player14);
                    allPlayers.put(player15.getName(),player15);
                    allPlayers.put(player16.getName(),player16);
                    allPlayers.put(player17.getName(),player17);
                    allPlayers.put(player18.getName(),player18);
                    allPlayers.put(player19.getName(),player19);
                    allPlayers.put(player20.getName(),player20);

                    Iterator<Map.Entry<String,Player>> iterator = allPlayers.entrySet().iterator();
                    while(iterator.hasNext()){
                        Map.Entry<String,Player> playerEntry = iterator.next();
                        Player player = playerEntry.getValue();
                        player.setAverageScore((double) (player.getRunsScored()/player.getMatchesPlayed()));
                    }

            }





            break;
//
                case 4: System.out.println("Final Team Players Information");








            }
        }while(choice!=5);
            oos.flush();
            oos.close();

            fos.close();
    }
}

